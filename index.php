<?php require_once __DIR__ . "/vendor/autoload.php";

$route = array_values(array_diff(explode("/", htmlspecialchars($_SERVER['PHP_SELF'])), array("", "index.php")));
$incidents = parse_ini_file(__DIR__ . "/data/content/incidents.ini", true);
$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

$twig->display(
    'counter.twig',
    [
        "title" => "The Foxtrot Counter",
        "incidents" => $incidents
    ]
);